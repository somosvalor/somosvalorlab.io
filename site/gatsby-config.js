module.exports = {
  siteMetadata: {
    title: `Somos Valor`,
    description: `Tecnologia e inteligência em soluções de cobrança!`,
    author: `@gatsbyjs`,
    pathPrefix: `/site`,
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-webpack-size`,
    { resolve: `gatsby-source-filesystem`, options: { path: `${__dirname}/src/images` } },
    { 
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `https://somosvalor.com.br/teste/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
