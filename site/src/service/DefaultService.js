import { ApiService } from './ApiService';
const endpoint = 'defaults';

const DefaultService = {
    async getFilter(filter){
        return await ApiService.getFilter(endpoint, filter);
    },
    list(){
        return ApiService.get(endpoint);
    },
    async create(item){
        return await ApiService.post(endpoint, item);
    },
    async update(item){
        return await ApiService.put(endpoint, item);
    },
    async remove(id){
        return await ApiService.delete(endpoint, id);
    }
}
export default DefaultService;