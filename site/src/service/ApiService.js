import axios from "axios";

// const url = 'http://localhost:4000/';
const url = 'https://api.github.com/repositories?since=364';

export const ApiService = {
    async getFilter(endpoint, filter){
      try {
        const response = await axios.get(`${url}${endpoint}?${filter}`);
        console.log(response);
      } catch (error) {
        console.error(error);
      }
    },
    async get(endpoint){
      try {
        // const response = await axios.get(`${url}${endpoint}`);
        const response = await axios.get(`${url}`);
        console.log(response);
      } catch (error) {
        console.error(error);
      }
    },
    async post(endpoint, data){
      const myHeaders = new Headers({
          "Content-Type": "application/json"
      });

      axios.post(`${url}${endpoint}`, data)
      .then(function (response) {
        console.log(responseresponse.json());
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    async put(endpoint, data){
        const myHeaders = new Headers({
            "Content-Type": "application/json"
        });
        return fetch(`${url}${endpoint}/${data.id}`, {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify(data)
        })
            .then(response => response.json());
    },
    async delete(endpoint, id){
        return fetch(`${url}${endpoint}/${id}`, {
            method: 'DELETE'
        })
            .then(response => response.json());
    }
}