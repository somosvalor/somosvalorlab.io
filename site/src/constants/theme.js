export const primaryFont = 'Open Sans';
export const secundaryFont = 'Caveat';

export const pink = '#FF4267';
export const pinkDark = '#d4084e';
export const red = '#F34B4B';
export const orange = '#F58A1F';
export const blueDark = '#203b5e';
export const powderBlue = '#9ED0DD';
export const regalBlue = '#1C3766';
export const prussianBlue = '#182439';
export const beige = '#FFE0CA';
export const white = '#ffffff';

export const accent = pink;

export default {
  primaryFont,
  secundaryFont,
  pink,
  pinkDark,
  red,
  orange,
  blueDark,
  powderBlue,
  regalBlue,
  prussianBlue,
  white,
};
