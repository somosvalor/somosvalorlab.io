export default {
  LARGE: 1600,
  DESKTOP: 1200,
  TABLET: 960,
  PHONE: 480,
};
