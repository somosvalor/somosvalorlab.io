import React from "react";
import { Button, Form } from 'react-bootstrap';

import Container from './../Container';

import * as S from './styled'

function FormContato() {

  return (
    <Container>
      <S.Titulo>Cobrança personalizada e régua de cobrança exclusiva</S.Titulo>

      <S.Wrapper>
        <div>
          <p>Por que terceirizar a cobrança com a SOMOS VALOR?</p>
          <S.Lista>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Profissionais qualificados para a atividade;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Efetividade na negociação;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Múltiplos canais de comunicação;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Alta taxa de retorno;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Recuperação de crédito com maior agilidade;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Sem taxas ou mensalidades para sua empresa;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Aumento dos lucros;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Evita o desgaste da sua empresa com seu devedor;
                <span></span>
              </label>
            </li>
            <li>
              <label>
                <input type="checkbox" defaultChecked={true} /> Os títulos ficam na sua empresa;
                <span></span>
              </label>
            </li>
          </S.Lista>
        </div>
        <div>
        <Form>
          <Form.Group controlId="nome">
            {/* <Form.Label>Nome</Form.Label> */}
            <Form.Control type="text" placeholder="Digite o nome completo" />
          </Form.Group>

          <Form.Group controlId="email">
            {/* <Form.Label>E-mail</Form.Label> */}
            <Form.Control type="email" placeholder="Digite o seu e-mail" />
            <Form.Text className="text-muted">
              Não compartilharemos o seu e-mail com ninguém
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="empresa">
            {/* <Form.Label>Empresa</Form.Label> */}
            <Form.Control type="text" placeholder="Empresa" />
          </Form.Group>
          
          <Form.Group controlId="telefone">
            {/* <Form.Label>Telefone</Form.Label> */}
            <Form.Control type="text" placeholder="Telefone" />
          </Form.Group>

          <Form.Group controlId="cidade">
            {/* <Form.Label>Cidade</Form.Label> */}
            <Form.Control type="text" placeholder="Cidade" />
          </Form.Group>

          <Form.Group controlId="mensagem">
            {/* <Form.Label>Mensagem</Form.Label> */}
            <Form.Control as="textarea" rows="3" placeholder="Mensagem"/>
          </Form.Group>

          <Button variant="primary" type="submit">
            Enviar
          </Button>
        </Form>
        </div>
      </S.Wrapper>
    </Container>
  )
}

export default FormContato;
