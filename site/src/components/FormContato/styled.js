import styled from 'styled-components';

import { device } from '../../configs/device';

export const Titulo = styled.h3`
  text-align: center;
  padding: 25px 0px;
`;

export const Wrapper = styled.div`
  display: grid;
  column-gap: 5px;
  row-gap: 10px;
  width: 90%;
  
  @media ${device.mobileS} { 
    grid-template-columns: repeat(1, minmax(100%, 1fr));
  }  
  @media ${device.mobileM} { 
    grid-template-columns: repeat(1, minmax(100%, 1fr));
  }
  @media ${device.mobileL} { 
    grid-template-columns: repeat(1, minmax(100%, 1fr));
  }  
  @media ${device.tablet} { 
    grid-template-columns: repeat(2, minmax(48%, 1fr));
  }  
  @media ${device.laptop} { 
    grid-template-columns: repeat(2, minmax(48%, 1fr));
  }  
`;

export const Lista = styled.ul`
  list-style: none;
  margin-left: 0;

  li{

  }

  label {
        position: relative;
        color: #222;
        font-size: 14px;
        padding-left: 25px;
        cursor: pointer;
    }
    label input {
        opacity: 0;
    }
    span {
        position: absolute;
        top: -7px;
        left: 0;
        width: 25px;
        height: 25px;
        border: 2px solid #2a5f01;
        border-radius: 3px;
    }

    span::before {
        position: absolute;
        content: '';
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        transform: scale(0);
        transition: cubic-bezier(1,.01,1,0) .5s;
        
    }

    span::after {
        position: absolute;
        content: '';
        top: 2px;
        left: 7px;
        width: 8px;
        height: 14px;
        border-right: 4px solid #2a5f01;
        border-bottom: 4px solid #2a5f01;
        transform: rotate(45deg)scale(0);
        transition:cubic-bezier(1,.01,1,0) .6s;
    }
    label input:checked ~ span::before {
         transform: scale(1);
    }
    label input:checked ~ span::after {
         transform: rotate(45deg)scale(1);
    }
`;

export const Btn = styled.div`
  color: #EEE;
  background: #458612;
  border-bottom: 4px solid #76D95E;
  border-radius: 3px;
  
  margin: 20px 10px 20px 10px;
  padding: 12px 20px 12px 20px;
  min-width: 156px;
  
  display: block;
  text-align: center;
  font-weight: 500;
  text-transform: uppercase;
  cursor: pointer;
  
  &:hover {
    background: #407d10;
    transition: 0.3s;
    border-bottom: 4px solid #64c34d;
  }
`;
