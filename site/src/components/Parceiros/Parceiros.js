import React from "react";
import { Link } from 'gatsby';
import Button from './../Button';

import Container from '../Container';
import * as S from './styled'

function Parceiros() {
  return (
    <Container>
      <S.Titulo>Com quem você já pode negociar</S.Titulo>
      <S.LogoParceiros>
        <div><img src={'../../images/logo_alfa.png'} alt="Logo da Faculdade Alfa" /></div>
        <div><img src={'../../images/logo_unipar.png'} alt="Logo da Universidade Unipar" /></div>
      </S.LogoParceiros>
    </Container>
  )
};

export default Parceiros;