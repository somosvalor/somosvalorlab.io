import styled from 'styled-components';

import { device } from '../../configs/device';

export const Titulo = styled.h2`
  color: #000;
  font-size: 31px;
  margin-bottom: 90px;

  @media ${device.mobileS} { 
    margin-bottom: 37px;
  }

  @media ${device.mobileL} { 
    margin-bottom: 37px;
  }
`;

export const LogoParceiros = styled.div`
  display: flex;
  justify-content: space-around;
  width: 70%;
  margin: 20px 0px;

  @media ${device.mobileS} { 
    flex-direction: column;
    align-items: center;
    div {
      & + div {
        margin-top: 31px;
      }
    }
  }

  @media ${device.mobileL} { 
    flex-direction: column;
    align-items: center;

    div {
      & + div {
        margin-top: 31px;
      }
    }
  }

  @media ${device.laptop} { 
    flex-direction: row;
    align-items: center;
    div {
      & + div {
        margin-top: 0px;
      }
    }
  }
  
  img {
    @media ${device.mobileS} { 
      max-width: 175px;
      max-height: 50px;
      margin: auto;
    }
    
    @media ${device.mobileL} { 
      max-width: 175px;
      max-height: 50px;
      margin: auto;

      & + img {
        margin-top: 31px;
      }
    }

    @media ${device.tablet} { 
      max-width: 230px;
      max-height: 69px;
    }

    @media ${device.laptop} { 
      max-width: 363px;
    }
  }
`;
