import React from "react";
import { Link } from 'gatsby';
import Button from './../Button';

import Container from '../Container';
import * as S from './styled';

function NegocieDivida() {
  return (
    <S.ContainerFull>
      <S.Titulo>Pagar Dívidas é Fácil</S.Titulo>

      <S.WrapperBox>
        <S.Box>
          <S.TamanhoImgDefault>
            <S.Icon src={"../../images/face-scan.png"} alt="Faça o seu cadastro" ></S.Icon>
          </S.TamanhoImgDefault>
          <S.BoxTitulo>Faça seu cadastro na plataforma.</S.BoxTitulo>
        </S.Box>
        <S.Box>
          <S.TamanhoImgDefault>
            <S.Icon src={"../../images/document.png"} alt="Verifica se sua divida está conosco" ></S.Icon>
          </S.TamanhoImgDefault>
          <S.BoxTitulo>Veja se sua dívida esta conosco, avalie as propostas que preparamos para você.</S.BoxTitulo>
        </S.Box>
        <S.Box>
          <S.TamanhoImgDefault>
            <S.Icon src={"../../images/calculator.png"} alt="Veja as propostas e negocie" ></S.Icon>
          </S.TamanhoImgDefault>
          <S.BoxTitulo>Veja proposta ideal e negocie.</S.BoxTitulo>
        </S.Box>
        <S.Box>
          <S.TamanhoImgDefault>
            <S.Icon src={"../../images/transaction.png"} alt="Faça o pagamento de forma rápida e fácil" ></S.Icon>
          </S.TamanhoImgDefault>
          <S.BoxTitulo>Pronto assim, rápido e fácil.</S.BoxTitulo>
          <div>
            <Link to="/contato"><Button background="#FFF" color="#407d10" borderColor="#407d10" type="success">Clique e Negocie</Button></Link>
          </div>
        </S.Box>
      </S.WrapperBox>
    </S.ContainerFull>
  )
};

export default NegocieDivida;