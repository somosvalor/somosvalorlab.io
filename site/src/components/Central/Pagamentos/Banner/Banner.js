import React from "react";

import * as S from './styled'

export default () => (
  <S.Content>
    <S.Wrapper>
      <S.MainContainer>
        <S.StepsListing className="steps-listing">
          <ol>
            <li className="step-item active"><span>1</span></li>
            <li className="step-item"><span>2</span></li>
            <li className="step-item"><span>3</span></li>
          </ol>
          <div className="progress-line"></div>
        </S.StepsListing>
      </S.MainContainer>
        {/* <button class="step-btn prev-btn">Prev</button>
        <button class="step-btn next-btn">Next</button> */}
    </S.Wrapper>
  </S.Content>
);