import styled from 'styled-components';

import { device } from '../../../../configs/device';


export const Content = styled.div`
  background: #EDFAEB;
  min-height: 175px;
  width: 100%;
  margin: auto;
  background-image: url('../images/bannerDebitos.png');
  background-repeat: no-repeat;
  overflow: hidden;
  object-fit: cover !important;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-size: cover;
`;

export const Wrapper = styled.div`
  width: 90%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;

export const MainContainer = styled.div`
  width: 100%;
  max-width: 1170px;
`;

export const StepsListing = styled.div`
  ol {
    margin: 0 auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    .step-item::after {
      content: "";
      border-bottom: 6px solid #FFF;
      width: 100%;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: 2px;
    }

    .step-item{
      list-style-type: none;
      position: relative;
      flex: 0 0 33.33%;
      max-width: 33.333%;
    }

    .step-item:last-child::after{
      display: none;
    }

    .step-item:last-child{
      max-width: 0;
      flex: 0 0 0;
    }

    .step-item span {
      width: 60px;
      height: 60px;
      display: flex;
      background-color: #A8A8A8;
      border: 2px solid #FFF;
      color: #FFF;
      font-size: 22px;
      font-weight: bold;
      position: relative;
      z-index: 1;
      justify-content: center;
      align-items: center;
      border-radius: 50%;
    }

    .step-item.active span{
      background-color: #0B6514;
    }

    .progress-line {
      height: 5px;
      background-color: #6e2d6a;
      position: absolute;
      left: 2px;
      bottom: 0;
      width: 0;
      top: 17px;
      transition: 0.6s ease-in-out all;
    }
  }
`;