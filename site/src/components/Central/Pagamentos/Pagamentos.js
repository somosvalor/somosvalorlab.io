import React, { useEffect, useState } from "react";
import { Button, Form, Row, Col, Tab, Tabs } from 'react-bootstrap';

import * as S from './styled'

import DividaList from './DividaList';

function FormPrimeiraEtapa(props) {
  return (
    <S.WrapperForms>
      <S.ConfirmacaoDados>
        <h4>Confirmar seus dados!</h4>
        <p>Confirme os dados para</p>
        <p>calcularmos as melhores formas de pagamento</p>
      </S.ConfirmacaoDados>

      <Form>
        <Form.Group controlId="nome">
          {/* <Form.Label>Nome</Form.Label> */}
          <Form.Control type="text" placeholder="John Doe" />
        </Form.Group>

        <Form.Group controlId="telefone">
          {/* <Form.Label>Telefone</Form.Label> */}
          <Form.Control type="text" placeholder="(44) 99444-9999" />
        </Form.Group>

        <Form.Group controlId="email">
          <Form.Control type="email" placeholder="johndoe@gmail.com" />
        </Form.Group>

        <Button variant="success" onClick={props.nextStep.bind(this)}>
          Pagar Agora
        </Button>
      </Form>
    </S.WrapperForms>
  );
}

function FormSegundaParte(props){
  const [key, setKey] = useState('cartao');

  const [parcelas, setParcelas] = useState(0);
  console.log(key);

  return (
    <S.Pagamento>
       <Tabs
        id="controlled-tab-example"
        activeKey={key}
        onSelect={(k) => setKey(k)}
      >
        <Tab eventKey="cartao" title="Pagamento no cartão em até 12X">
          <S.ContentTab>
            <Form>
              <Form.Group controlId="nome">
                <Form.Label>Nome impresso no cartão</Form.Label>
                <Form.Control type="text" name="nome" placeholder="Digite o nome igual no cartão" />
              </Form.Group>

              <Form.Group controlId="numero">
                <Form.Label>Número do Cartão</Form.Label>
                <Form.Control type="text" name="numero" placeholder="1111 1111 1111 1111" />
              </Form.Group>
              
              <Row>
                <Col>
                <Form.Group controlId="data">
                    <Form.Label>Data de Expiração</Form.Label>
                    <Form.Control type="text" name="expiracao" placeholder="MM/AA" />
                  </Form.Group>
                </Col>
                <Col>
                <Form.Group controlId="codigo">
                    <Form.Label>Código</Form.Label>
                    <Form.Control type="text" name="codigo" placeholder="" />
                  </Form.Group>
                </Col>
              </Row>

              <Form.Group controlId="parcelas">
                <Form.Label>Parcelas</Form.Label>
                <Form.Control as="select" value={parcelas} onChange={(e) => setParcelas(e.target.value)} placeholder="Selecione as parcelas">
                  <option value="0">Selecione</option>
                  <option value="1">01</option>
                  <option value="2">02</option>
                  <option value="3">03</option>
                  <option value="4">04</option>
                  <option value="5">05</option>
                  <option value="6">06</option>
                  <option value="7">07</option>
                  <option value="8">08</option>
                  <option value="9">09</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </Form.Control>
              </Form.Group>
            </Form>
          </S.ContentTab>
        </Tab>
        <Tab eventKey="boleto" title="Pagamento AVISTA BOLETO 5% DE DESCONTO">
          <S.ContentTabBoleto>
            <h4>Jhon doe</h4>
            <p>Pagando com boleto você tem o desconto de 5% então o valor a pagar é de: </p>
            <S.PrecoBoleto>R$ 3.291,75</S.PrecoBoleto>
          </S.ContentTabBoleto>
        </Tab>
        {/* <Tab eventKey="contact" title="Contact" disabled>
          <p>Terceira parte</p>
        </Tab> */}
      </Tabs>
      <S.ButtonsFooter>
        <div>
          <Button variant="secondary" onClick={props.prevStep.bind(this)}>
            Voltar
          </Button>
        </div>
        <div>
          <Button variant="success" onClick={props.nextStep.bind(this)}>
            Pagar Agora
          </Button>
        </div>
      </S.ButtonsFooter>
    </S.Pagamento>
  )
}

function FormTerceiraParte(props){
  return (
    <S.TerceiraParte>
      <h4>Jhon Doe</h4>
      <p>Muito Obrigado!</p>
      <p>Pagamento Efetuado com sucesso.</p>
      <Button variant="success">Ver Comprovante</Button>
    </S.TerceiraParte>
  )
}

export default () => {

  const [stepsPayments, setStepsPayments] = useState(1);

  function nextStep(){
    if (stepsPayments < 3) {
      let addSteps = stepsPayments + 1;
      setStepsPayments(Number(addSteps));
      const steps = document.querySelector('.steps-listing');
      const allItems = document.querySelectorAll('.step-item.active')
      const lastItem = allItems[allItems.length - 1];

      if ( lastItem.nextElementSibling != null ) {
        lastItem.nextElementSibling.classList.add('active');
      }
    }    
  }
  
  function prevStep(){
    if (stepsPayments > 1) {
      let addSteps = stepsPayments - 1;
      setStepsPayments(Number(addSteps));

      const steps = document.querySelector('.steps-listing');
      const allItems = document.querySelectorAll('.step-item.active');
      const lastItem = allItems[allItems.length - 1];
      lastItem.classList.remove('active');
    }    
  }

  return (
    <S.Content>
      {
        stepsPayments == 1 ? <DividaList /> : ""
      }

      <div>
        {
          stepsPayments == 1 
          ?
            <FormPrimeiraEtapa nextStep={nextStep}></FormPrimeiraEtapa>
          :
            stepsPayments == 2 ? 
              <FormSegundaParte nextStep={nextStep} prevStep={prevStep}></FormSegundaParte> 
            : 
              <FormTerceiraParte></FormTerceiraParte>        
        }
      </div>
    </S.Content>
  )
};