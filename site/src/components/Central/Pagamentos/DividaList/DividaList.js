import React from "react";

import * as S from './styled'

export default () => (
  <S.Wrapper>
    <S.SubTexto>Dados da Dívida com</S.SubTexto>
    <S.NomeEmpresa>Faculdade Alfa</S.NomeEmpresa>

    <S.Lista>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 1 vencida em 10/01/2019
          <span></span>
        </label>
      </li>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 2 vencida em 10/02/2019
          <span></span>
        </label>
      </li>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 3 vencida em 10/03/2019
          <span></span>
        </label>
      </li>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 4 vencida em 10/04/2019
          <span></span>
        </label>
      </li>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 5 vencida em 10/05/2019
          <span></span>
        </label>
      </li>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 6 vencida em 10/06/2019
          <span></span>
        </label>
      </li>
      <li>
        <label>
          <input type="checkbox" defaultChecked={true} /> R$ 450,00 Parcela 7 vencida em 10/07/2019
          <span></span>
        </label>
      </li>
    </S.Lista>

    <S.Calculo>
      <div>
        SubTotal:
      </div>
      <div>
        R$ 3.150,00
      </div>
    </S.Calculo>
    <S.Calculo>
      <div>
        Juros+Multas:
      </div>
      <div>
        R$ 315,00
      </div>
    </S.Calculo>
    <S.Calculo>
      <S.Total>
        Total:
      </S.Total>
      <S.Total>
        R$ 3.465,00
      </S.Total>
    </S.Calculo>
  </S.Wrapper>
);