import styled from 'styled-components';

import { device } from '../../../configs/device';

export const Content = styled.div`
  background: #EDFAEB;
  min-height: 442px;
  width: 90%;
  padding: 40px 10px;
  margin: auto;
  display: flex;
  justify-content: space-around;
`;

export const WrapperForms = styled.div`
  form {
    button{
      width: 100%;
      font-weight: bold;
      text-transform: uppercase;
    }
  }
`;

export const ContentTab = styled.div`
    display: flex;
    justify-content: center;
    padding: 25px 0px;
`;

export const ContentTabBoleto = styled.div`
    padding: 25px 0px;
    text-align: center;

    h4 {
      color: #0B6514;
      font-weight: bold;
      text-transform: uppercase;
    }

    p {
      color: #5E5E5E;
      font-weight: 600;
      text-transform: uppercase;
      margin-bottom: 0px;
      width: 65%;
      padding: 25px 0px;
      margin: 0 auto;
    }
`;

export const PrecoBoleto = styled.h2`
  color: #0B6514;
  font-weight: bold;
  text-transform: uppercase;
`;

export const ConfirmacaoDados = styled.div`
  text-align: center;
  padding-bottom: 20px;

  h4 {
    color: #193A1C;
    font-weight: bold;
    padding-bottom: 10px;
  }
  p {
    color: #797979;
    margin-bottom: 0px;
  }
`;

export const Pagamento = styled.div`

  .nav-tabs .nav-link {
    background: #DADAE0;
    color: #000;
    font-weight: 600;
  }

  .nav-tabs .nav-link.active {
    border-bottom: 3px solid #458612;
    background: #FFF;
    font-weight: 600;
  }
`;

export const ButtonsFooter = styled.div`
  display: flex;
  justify-content: space-between;

  div {
    button{
      text-transform: uppercase;
      font-weight: 600;
    }
  }
`;

export const TerceiraParte = styled.div`
  text-align: center;
  h4 {
    color: #0B6514;
    font-weight: bold;
  }
  p {
    color: #5E5E5E;
    text-transform: uppercase;
    font-weight: 600;
    margin-bottom: 0px;
  }

  button {
    text-transform: uppercase;
    font-weight: 600;
    margin-top: 15px;
  }
`;