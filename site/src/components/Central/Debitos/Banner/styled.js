import styled from 'styled-components';

import { device } from '../../../../configs/device';


export const Content = styled.div`
  background: #EDFAEB;
  min-height: 175px;
  width: 100%;
  margin: auto;
  background-image: url('../images/bannerDebitos.png');
  background-repeat: no-repeat;
  overflow: hidden;
  object-fit: cover !important;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-size: cover;
`;

export const Wrapper = styled.div`
  width: 90%;
`;

export const Titulo = styled.h2`
  color: #FFF;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;

  @media ${device.mobileS} { 
    font-size: 20px;
  } 
  @media ${device.mobileS} { 
    font-size: 16px;
  }  
  @media ${device.mobileM} { 
    font-size: 18px;
  }  
  @media ${device.mobileL} { 
    font-size: 20px;
  }  
  @media ${device.tablet} { 
    font-size: 22px;
  }  
  @media ${device.laptop} { 
    font-size: 27px;
  }
`;