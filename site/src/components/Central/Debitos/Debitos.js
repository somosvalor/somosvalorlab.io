import React from "react";

import Button from '../../Button';

import * as S from './styled'

export default () => (
  <S.Content>
    <S.Wrapper>
      <S.Box>
        <S.Empresa>
          <p>Debito Faculdade Alfa</p>
        </S.Empresa>
        <S.Data>
          <p>Atrazado desde</p>
          <p>10/09/2019</p>
        </S.Data>
        <S.Preco>
          R$ 2800,00
        </S.Preco>
        <Button>Pagar</Button>
      </S.Box>
      <S.Box>
        <S.Empresa>
          <p>Debito Faculdade Alfa</p>
        </S.Empresa>
        <S.Data>
          <p>Atrazado desde</p>
          <p>10/09/2019</p>
        </S.Data>
        <S.Preco>
          R$ 2800,00
        </S.Preco>
        <Button>Pagar</Button>
      </S.Box>
      <S.Box>
        <S.Empresa>
          <p>Debito Faculdade Alfa</p>
        </S.Empresa>
        <S.Data>
          <p>Atrazado desde</p>
          <p>10/09/2019</p>
        </S.Data>
        <S.Preco>
          R$ 2800,00
        </S.Preco>
        <Button>Pagar</Button>
      </S.Box>
      <S.Box>
        <S.Empresa>
          <p>Debito Faculdade Alfa</p>
        </S.Empresa>
        <S.Data>
          <p>Atrazado desde</p>
          <p>10/09/2019</p>
        </S.Data>
        <S.Preco>
          R$ 2800,00
        </S.Preco>
        <Button>Pagar</Button>
      </S.Box>
      <S.Box>
        <S.Empresa>
          <p>Debito Faculdade Alfa</p>
        </S.Empresa>
        <S.Data>
          <p>Atrazado desde</p>
          <p>10/09/2019</p>
        </S.Data>
        <S.Preco>
          R$ 2800,00
        </S.Preco>
        <Button>Pagar</Button>
      </S.Box>
      <S.Box>
        <S.Empresa>
          <p>Debito Faculdade Alfa</p>
        </S.Empresa>
        <S.Data>
          <p>Atrazado desde</p>
          <p>10/09/2019</p>
        </S.Data>
        <S.Preco>
          R$ 2800,00
        </S.Preco>
        <Button>Pagar</Button>
      </S.Box>
    </S.Wrapper>
  </S.Content>
);