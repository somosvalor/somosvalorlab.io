import styled from 'styled-components';

import { device } from '../../../configs/device';

export const Content = styled.div`
  background: #EDFAEB;
  min-height: 442px;
  width: 90%;
  padding: 40px 0px;
  margin: auto;
`;

export const Wrapper = styled.div`
  display: grid;
  column-gap: 10px;
  row-gap: 15px;

  @media ${device.mobileS} { 
    grid-template-columns: repeat(1, minmax(168px, 1fr));
  }  
  @media ${device.mobileM} { 
    grid-template-columns: repeat(1, minmax(168px, 1fr));
  }
  @media ${device.mobileL} { 
    grid-template-columns: repeat(1, minmax(120px, 1fr));
  }  
  @media ${device.tablet} { 
    grid-template-columns: repeat(3, minmax(120px, 1fr));
  }  
  @media ${device.laptop} { 
    grid-template-columns: repeat(3,minmax(180px,1fr));
  }  
  
`;

export const Box = styled.div`
  border: 1px solid #D3D3D3;
  border-radius: 4px;
  padding: 20px 25px;

  text-align: center;
`;

export const Empresa = styled.div`
  color: #0B6514;
  font-weight: 600;
  text-transform: uppercase;
`;

export const Data = styled.div`
  color: #797979;
  line-height: 35%;
  font-size: 21px;
`;

export const Preco = styled.div`
  color: #1CC6EB;
  font-weight: bold;
  font-size: 31px;
`;

