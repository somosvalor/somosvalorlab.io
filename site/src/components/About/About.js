import React from "react";

import eticaExcelencia from '../../images/eticaExcelencia.png';
import acoesEstrategicas from '../../images/acoesEstrategicas.png';
import eficienciaInovacao from '../../images/eficienciaInovacao.png';

import * as S from './styled'

export default () => (
  <S.Content>
    <S.Wrapper>
      <S.Titulo>
        "Somos Valor nasceu para facilitar a vida de empresas e pessoas do mal que assola os negócios: a inadimplência."
      </S.Titulo>
      <S.WrapperBox>
        <S.Box>
          <S.Icon src={"../../images/eticaExcelencia.png"} alt="Ética e Excelência" ></S.Icon>
          <S.BoxTitulo>Ética e Excelência</S.BoxTitulo>
          <S.Descricao>Nossos colaboradores recebem treinamento contínuo e são premiados por excelência e desempenho.</S.Descricao>
        </S.Box>
        <S.Box>
          <S.Icon src={"../../images/acoesEstrategicas.png"} alt="Ações estratégicas" ></S.Icon>
          <S.BoxTitulo>Ações estratégicas</S.BoxTitulo>
          <S.Descricao>Temos uma régua de cobrança para cada cliente, o que otimiza resultados.</S.Descricao>
        </S.Box>
        <S.Box>
          <S.Icon src={"../../images/eficienciaInovacao.png"} alt="Eficiência e inovação" ></S.Icon>
          <S.BoxTitulo>Eficiência e inovação</S.BoxTitulo>
          <S.Descricao>Nossa atuação é personalizada com base na sua atividade empresarial e no tipo de dívida a receber.</S.Descricao>
        </S.Box>
      </S.WrapperBox>
    </S.Wrapper>
  </S.Content>
);