import styled from 'styled-components';

import { device } from '../../configs/device';

export const Content = styled.div`
  background: #EDFAEB;
  min-height: 442px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  padding-top: 40px;
  margin: auto;

  @media ${device.mobileS} { 
    min-height: 350px;
  }

  @media ${device.mobileL} { 
    min-height: 350px;
  }  
`;

export const Titulo = styled.h2`
  color: #193A1C;
  font-weight: bold;
  text-align: center;
  @media ${device.mobileS} { 
    font-size: 20px;
  } 
  @media ${device.mobileS} { 
    font-size: 16px;
  }  
  @media ${device.mobileM} { 
    font-size: 18px;
  }  
  @media ${device.mobileL} { 
    font-size: 20px;
  }  
  @media ${device.tablet} { 
    font-size: 22px;
  }  
  @media ${device.laptop} { 
    font-size: 27px;
    width: 70%;
  }
`;

export const WrapperBox = styled.div`
  display: grid;
  column-gap: 10px;
  row-gap: 15px;
  padding-top: 35px;

  @media ${device.mobileL} { 
    grid-template-columns: repeat(1, minmax(210px, 1fr));
  }  
  @media ${device.tablet} { 
    grid-template-columns: repeat(3, minmax(227px, 1fr));
  }  
  @media ${device.laptop} { 
    grid-template-columns: repeat(3,minmax(312px,1fr));
  }  
`;

export const Box = styled.div`
  display: grid;
  justify-items: center;

  color: #193A1C;
`;

export const Icon = styled.img`
  @media ${device.mobileS} { 
    max-width: 210px;
    max-height: 150px;
  } 
  @media ${device.mobileM} { 
    max-width: 210px;
    max-height: 150px;
  } 
  @media ${device.mobileL} { 
    max-width: 210px;
    max-height: 150px;
  } 
  @media ${device.tablet} { 
    max-width: 206px;
    max-height: 157px;
  }  
  @media ${device.laptop} { 
    max-width: 235px;
    max-height: 153px;
  }  
`;

export const BoxTitulo = styled.p`
  font-weight: 600;
`;
export const Descricao = styled.p`
  min-height: 65px;
  text-align: center;

  @media ${device.tablet} { 
    font-size: 13px;
  }  
`;