import React from "react"
import PropTypes from "prop-types"

import Fade from 'react-reveal/Fade';

import * as S from './styled'

const Header = ({ siteTitle }) => {
  return (
    <S.HeaderCustom>
      <S.Wrapper>
        <S.Menu>
          <div className="nav">
            <input type="checkbox" id="nav-check" />
            <Fade left>
              <div className="nav-header">
                <div className="nav-title">
                <S.LogoLink href="/">
                  <img src={'../../../images/logo.png'} alt={siteTitle} />
                </S.LogoLink>
                </div>
              </div>
            </Fade>
            <div className="nav-btn">
              <label htmlFor="nav-check">
                <span></span>
                <span></span>
                <span></span>
              </label>
            </div>
            <div className="nav-links">
              <Fade right>
                <div>
                  <a href="/teste">Início</a>
                  <a href="/teste/contato" title="Seja um Parceiro da Somos Valor">Seja um Parceiro</a>
                  <a href="/teste/quem-somos" title="soms valor - Quem somos">Quem Somos</a>
                  <a href="/teste/blog" title="Somos valor - Blog">Blog</a>
                  <a href="/teste/assepro" title="Somos valor - Assepro">Empresa Associada a Assepro</a>
                  {/* <a href="/teste/empresas">Para Empresas</a>
                  <a href="/teste/pessoas">Para Pessoas</a> */}
                  <a className="login" href="#">Entrar</a>
                </div>
              </Fade>
            </div>
          </div>
        </S.Menu>
      </S.Wrapper>
    </S.HeaderCustom>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
