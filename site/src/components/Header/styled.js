import styled from 'styled-components';

export const HeaderCustom = styled.header`
  background: #FFFFFF;
  height:92px;
  box-shadow: 0 0 10px rgba(0,0,0,0.5);

  position: fixed;
  top: 0;
  width: 100%;
  z-index: 9;
`;

export const Wrapper = styled.div`

`;

export const LogoLink = styled.a`
  height: 92px;
  max-height: 72px;
  margin: 0;
  img {
    height: 80px;
  }
`;

  export const Menu = styled.menu`
    .nav {
      height: 92px;
      width: 100%;
      position: relative;

      display: flex;
      justify-content: space-around;
      align-items: center;

      .nav-header {
        display: inline;

        .nav-title {
          display: inline-block;
          font-size: 22px;
          color: #fff;
        }
      }

      .nav-btn {
        display: none;
      }

      .nav-links {
        display: inline;
        float: right;
        font-size: 14px;

        .login{
          color: #EEE;
          background: #0B6514;
          border-radius: 5px;
          box-shadow: 0px 4px 2px #777777;

          color: #FFF;
          
          margin: 0px;
          padding: 8px 20px 8px 20px;
          min-width: 116px;
          width: 116px;
          
          text-align: center;
          font-weight: 500;
          text-transform: uppercase;
          cursor: pointer;
          
          &:hover {
            background: #0B6514;
            color: #FFF;
            transition: 0.3s;
          }
        }

        a {
          margin: 0 10px;
          text-decoration: none;
          color: #19301c;
          text-transform: uppercase;
          font-weight: bold;
          &:hover {
            color: #2a5f01;
            transition: 0.3s;
          }
        }
      }

      #nav-check {
        display: none;
      }

      @media (max-width:745px) {
        justify-content: space-between;

        .nav-btn {
          display: inline-block;
          right: 0px;
          top: 0px;

          label {
            display: inline-block;
            padding: 13px;
            cursor: pointer;

            span {
              display: block;
              width: 25px;
              height: 10px;
              border-top: 2px solid #2a5f01;
            }
          }
        }

        .nav-links {
          position: absolute;
          display: block;
          width: 100%;
          background-color: #FFF;
          height: 0px;
          transition: all 0.3s ease-in;
          overflow-y: hidden;
          top: 92px;
          left: 0px;
          z-index: 999;
          text-align: center;

          .login {
            display: block;
            width: 100%;
            border-bottom: 1px solid #45861226;
          }

          a {
            display: block;
            width: 100%;
            padding: 15px 0px;
            border-bottom: 1px solid #45861226;
          }
        }

        #nav-check:not(:checked) ~ .nav-links {
          height: 0px;
        }
        #nav-check:checked ~ .nav-links {
          height: calc(100vh - 50px);
          overflow-y: auto;
        }
      }
    }
  `;

