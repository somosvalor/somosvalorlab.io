import React from "react";
import { Link } from 'gatsby';
import Button from './../Button';

import Container from '../Container';
import * as S from './styled'

function Solucoes() {
  return (
      <S.ContainerFull>
          <S.Titulo>Soluções Financeiras</S.Titulo>
          <S.SubTitulo>Conheça um pouco dos nossos serviços disponíveis</S.SubTitulo>

          <S.WrapperBox>
            <S.Box>
              <S.Icon src={"../../images/mini_banner_boleto.png"} alt="Faça o seu cadastro" ></S.Icon>
              <S.BoxTitulo>Boleto</S.BoxTitulo>
              <S.Descricao>Emissão de boletos registrados...</S.Descricao>
            </S.Box>
            <S.Box>
              <S.Icon src={"../../images/mini_banner_solucoes_financeiras.png"} alt="Faça o seu cadastro" ></S.Icon>
              <S.BoxTitulo>BI</S.BoxTitulo>
              <S.Descricao>Inteligência aplicada ao seu negócio...</S.Descricao>
            </S.Box>
            <S.Box>
              <S.Icon src={"../../images/mini_banner_cobranca_amigavel.png"} alt="Faça o seu cadastro" ></S.Icon>
              <S.BoxTitulo>Cobrança Amigáveis</S.BoxTitulo>
              <S.Descricao>Trabalhando com a formulação de acordos...</S.Descricao>
            </S.Box>
            <S.Box>
              <S.Icon src={"../../images/mini_banner_cobranca_judicial.png"} alt="Faça o seu cadastro" ></S.Icon>
              <S.BoxTitulo>Cobranças Judicial</S.BoxTitulo>
              <S.Descricao>Oferecemos...</S.Descricao>
            </S.Box>
          </S.WrapperBox>
      </S.ContainerFull>
  )
};

export default Solucoes;