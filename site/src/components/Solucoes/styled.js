import styled from 'styled-components';

import { device } from '../../configs/device';

export const ContainerFull = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  padding: 20px 0px;
  margin: 0 auto;
`;

export const Titulo = styled.h2`
  font-size: 31px;
`;
export const SubTitulo = styled.h5`
  
`;

export const WrapperBox = styled.div`
  display: grid;
  column-gap: 10px;
  row-gap: 15px;
  padding-top: 35px;

  @media ${device.mobileL} { 
    grid-template-columns: repeat(1, minmax(210px, 1fr));
  }  
  @media ${device.tablet} { 
    grid-template-columns: repeat(2, minmax(177px, 1fr));
  }  
  @media ${device.laptop} { 
    grid-template-columns: repeat(4,minmax(277px,1fr));
  }  
`;

export const Box = styled.div`
  display: grid;
  justify-items: center;

  color: #193A1C;
`;

export const Icon = styled.img`
  max-width: 147px;
  max-height: 153px;
`;

export const BoxTitulo = styled.p`
  font-weight: 600;
`;
export const Descricao = styled.p`
  min-height: 65px;
  text-align: center;

  @media ${device.tablet} { 
    font-size: 13px;
  }  
`;
