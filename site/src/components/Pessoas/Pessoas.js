import React from "react";
import { Form } from 'react-bootstrap';

import Container from '../Container';

import * as S from './styled';

function Pessoas() {

  return (
    <Container>
      <S.Wrapper>
        <div>
          <S.Titulo>Para pessoas</S.Titulo>
          <S.Text>
            Muito se discute sobre a importância do relacionamento com o cliente. Porém é preciso destacar que essa é uma necessidade de todas as áreas, o que inclui o setor de cobranças.
          </S.Text>
          <S.Text>
            Conseguir desenvolver um diálogo eficiente e assertivo com o devedor é uma premissa para o alcance das metas estabelecidas e evita que o relacionamento seja rompido. Afinal, cobrança não pode interromper um ciclo de confiança e parceria. 
          </S.Text>
          <S.Text>
            Estar endividado, na maioria das vezes, não é uma escolha pessoal.
          </S.Text>
          <S.Text>Diversas questões levam pessoas a complicarem sua vida financeira.</S.Text>
          <S.Text>A Somos Valor cobranças trabalha de forma discreta e sempre atenta ao Código de Defesa do Consumidor.</S.Text>          
        </div>
        <div>
          <img src={'../../images/pessoas.png'} alt="Para Pessoas" />
        </div>
      </S.Wrapper>
    </Container>
  )
}

export default Pessoas;
