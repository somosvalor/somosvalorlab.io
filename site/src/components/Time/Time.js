import React, { useEffect } from "react";
import { Link } from 'gatsby';
import Button from './../Button';

import Container from '../Container';
import * as S from './styled';

function Time() {

  function animateValue(id, start, end, duration) {

    var obj = document.getElementById(id);
    var range = end - start;
    var minTimer = 50;
    var stepTime = Math.abs(Math.floor(duration / range));

    stepTime = Math.max(stepTime, minTimer);
    
    var startTime = new Date().getTime();
    var endTime = startTime + duration;
    var timer;
  
    function run() {
        var now = new Date().getTime();
        var remaining = Math.max((endTime - now) / duration, 0);
        var value = Math.round(end - (remaining * range));
        obj.innerHTML = value;
        if (value == end) {
            clearInterval(timer);
        }
    }
    
    timer = setInterval(run, stepTime);
    run();
}

  useEffect(()=> {
      animateValue('clientes-atendidos',0, 200, 5000)
      animateValue('tradicao',0, 2, 5000)
      animateValue('colaboradores',0, 100, 5000)
  }, [])

  return (
    <S.ContainerFull>
      <S.Content>
        <div>
          <S.Titulo>Faça parte desse Time</S.Titulo>
        </div>
        <S.Wrapper>
          <S.Box>
            <h4 id="clientes-atendidos"></h4>
            <h6>Clientes atendidos</h6>
          </S.Box>
          <S.Box>
            <h4 id="tradicao"></h4>
            <h6>Anos de tradição</h6>
          </S.Box>
          <S.Box>
            <h4 id="colaboradores"></h4>
            <h6>Colaboradores</h6>
          </S.Box>
        </S.Wrapper>
      </S.Content>
    </S.ContainerFull>
  )
};

export default Time;