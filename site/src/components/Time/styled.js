import styled, { keyframes } from 'styled-components';

import { device } from '../../configs/device';

export const ContainerFull = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 20px 30px;
  background-image: url('../../images/estatisticas_somosvalor.png');
  background-size: cover;
  /* background-image: linear-gradient(120deg, #64c34d 0%, #0B6514 100%), linear-gradient(120deg, #64c34d 0%, #0B6514 100%); */
`;

export const Content = styled.div`
    padding: 35px 0px;
`;

export const Titulo = styled.h2`
  font-size: 31px;
  color: #FFF;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  color: #FFF;
  padding-top: 25px;

  @media ${device.mobileL} { 
    flex-direction: column;
  }  
  @media ${device.tablet} { 
    flex-direction: row;
  }  
  @media ${device.laptop} { 
    flex-direction: row;
  } 
`;

export const Box = styled.div`
  /* border: 1px solid #cccccc57;
  border-radius: 4px; */
  padding: 20px 15px;
  margin-right: 10px;

  text-align: center;

  h4 {
    font-size: 45px;
    font-weight: bold;
  }

  h6 {
    font-weight: 500;
  }

  @media ${device.mobileL} { 
    margin-top: 20px;
  }  
  @media ${device.tablet} { 
    margin-top: 0px;
  }  
  @media ${device.laptop} { 
    margin-top: 0px;
  } 
`;
