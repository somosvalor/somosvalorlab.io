import styled from "styled-components";

const Button = styled.div`
  color: ${props => props.color || "#EEE"};
  background: ${props => props.background || "#0B6514"};
  border: 1px solid ${props => props.borderColor || "#0B6514"};
  border-radius: 4px;
  
  margin: ${props => props.margin || "0px"};
  padding: ${props => props.padding || "12px 20px 12px 20px"};
  min-width: ${props => props.width || "156px"};
  width: ${props => props.width || "100%"};
  
  display: block;
  text-align: center;
  font-weight: 500;
  text-transform: uppercase;
  cursor: pointer;
  
  &:hover {
    background: ${props => props.background || "#0B6514"};
    transition: 0.3s;
    color: ${props => props.color || "#EEE"};
  }
`;

export default Button;