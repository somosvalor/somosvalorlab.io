import React, { useEffect, useState } from "react";
import { Link } from 'gatsby';

import Button from './../Button';

import { Form } from 'react-bootstrap';

import * as S from './styled'

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

import DefaultService from './../../service/DefaultService';

const CarouselRotate = () => {

  const getTeste = async () => {
    const filter = await DefaultService.list();
    console.log(filter);
    return filter;
  }

  useEffect(() => {
    getTeste();
  }, [])
 
  return(
    <Carousel 
      // autoPlay 
      showStatus={false} 
      showIndicators={false} 
      showThumbs={false} 
      infiniteLoop={true}
      dynamicHeight={false}
    >
      <S.Item>
        <S.Wrapper>
          <div>
            <S.Titulo><span>Renegocie</span> suas dívidas agora com as <span>melhores taxas!</span></S.Titulo>
          </div>
          <S.Box>
            <h5>Consulte grátis ofertas para suas dívidas e negocie online</h5>
            <div>

            <Form>
              <Form.Group controlId="nome">

                <S.WrapperInputbutton>
                  <Form.Control type="text" placeholder="Informe seu CPF" />
                  <Button variant="primary" width="156px" padding="4px 0px 4px 0px" margin="20px 0px 0px 10px;" type="submit">
                    Consultar
                  </Button>
                </S.WrapperInputbutton>
                
              </Form.Group>
            </Form>
            </div>
          </S.Box>
        </S.Wrapper>
        <S.ImageCarousel/>
      </S.Item>
      {/* <S.Item>
        <S.Wrapper>
          <div>
            <S.Titulo>Somos a tecnologia aliada com o progresso da sua empresa</S.Titulo>
            <S.ContentButtonParceiro>
              <Link to="/contato"><Button width="226px" type="success">Seja um Parceiro</Button></Link>  
            </S.ContentButtonParceiro>
          </div>
          <S.Box>
            <h5>Consulte grátis ofertas para suas dívidas e negocie online</h5>
            <div>

            <Form>
              <Form.Group controlId="nome">

                <S.WrapperInputbutton>
                  <Form.Control type="text" placeholder="Informe seu CPF" />
                  <Button variant="primary" padding="4px 0px 4px 0px" margin="20px 0px 0px 10px;" type="submit">
                    Consultar
                  </Button>
                </S.WrapperInputbutton>
                
              </Form.Group>
            </Form>
            </div>
          </S.Box>
        </S.Wrapper>
        <S.ImageCarousel alt="Somos a tecnologia aliada com o progresso da sua empresa!" src={'../../images/sejaParceiro.png'} />
      </S.Item> */}
    </Carousel>
  );
}
export default CarouselRotate;