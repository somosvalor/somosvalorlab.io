import styled, { css } from 'styled-components';

import { device } from '../../configs/device';

export const Item = styled.div`
  position:relative;
`;
export const Wrapper = styled.div`
  position:absolute;
  display: flex;
  justify-content: center;
  flex-direction: row;
  align-items: center;
  width: 10000; 
  height: 60vh;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 100;
  box-sizing: border-box;
  padding: 40px 20px;
  background: #193a1c;

  @media ${device.mobileS} { 
    flex-direction: column;
  }
  @media ${device.mobileM} { 
    flex-direction: column;
  }
  
  @media ${device.mobileL} { 
    flex-direction: column;
  }
  
  @media ${device.tablet} { 
    flex-direction: column;
  }
  @media ${device.laptop} { 
    flex-direction: row;
  }
`;
export const Titulo = styled.h2`
  color: #FFFFFF;
  width: 100%;
  text-align: center;
  span{
    color: #76D95E;
  }

  @media ${device.mobileS} { 
    font-size: 20px;
  }
  @media ${device.mobileM} { 
    font-size: 20px;
  }
  
  @media ${device.mobileL} { 
    font-size: 26px;
  }
  
  @media ${device.tablet} { 
    font-size: 27px;
  }
  @media ${device.laptop} { 
    font-size: 34px;
  }
`;
export const ImageCarousel = styled.div`
  width: 100vw !important;
  object-fit: cover !important;
  height: 60vh !important;
`;

const width = '100vw', height='60vh';
export const Container = styled.div`
  position: relative;
  overflow: hidden;
  width: ${width};
  height: ${height};
  object-fit: cover !important;
`;
export const Arrow = styled.div`
  text-shadow: 1px 1px 1px #fff;
  z-index: 100;
  line-height: ${height};
  text-align: center;
  position: absolute;
  top: 0;
  width: 10%;
  font-size: 3em;
  cursor: pointer;
  user-select: none;
  ${props => props.right ? css`left: 90%;` : css`left: 0%;`}
`;

export const Box = styled.div`
  border-radius: 9px;
  padding: 20px;
  background: #FFF;
  box-shadow:0 0 1em black;
  max-width: 533px;
  margin-right: 20px;
  margin-top: 20px;

  h5 {
    color: #193A1C;
    text-align: left;
    padding-bottom: 25px;
  }
`;

export const WrapperInputbutton = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  @media ${device.mobileS} { 
    flex-direction: column;
  }
  @media ${device.mobileM} { 
    flex-direction: column;
  }
  
  @media ${device.mobileL} { 
    flex-direction: column;
  }
  
  @media ${device.tablet} { 
    flex-direction: column;
  }
  @media ${device.laptop} { 
    flex-direction: row;

    input {
      margin-top: 20px;
    }
  }
`;

export const ContentButtonParceiro = styled.div`
  display: flex;
  justify-content: center;
`;
