import styled, { keyframes } from 'styled-components';

import { device } from '../../configs/device';

export const ContainerFull = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  padding: 20px 0px;
  margin: 0 auto;
`;

export const Titulo = styled.h2`
  font-size: 31px;
`;

export const Content = styled.div`
  padding-top: 30px;
`;

export const Lista = styled.ul`
  list-style: none;
  margin-left: 0;

  li{

  }

  label {
        position: relative;
        color: #222;
        font-size: 14px;
        padding-left: 25px;
        cursor: pointer;
    }
    label input {
        opacity: 0;
    }
    span {
        position: absolute;
        top: -7px;
        left: 0;
        width: 25px;
        height: 25px;
        border: 2px solid #2a5f01;
        border-radius: 3px;
    }

    span::before {
        position: absolute;
        content: '';
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        transform: scale(0);
        transition: cubic-bezier(1,.01,1,0) .5s;
        
    }

    span::after {
        position: absolute;
        content: '';
        top: 2px;
        left: 7px;
        width: 8px;
        height: 14px;
        border-right: 4px solid #2a5f01;
        border-bottom: 4px solid #2a5f01;
        transform: rotate(45deg)scale(0);
        transition:cubic-bezier(1,.01,1,0) .6s;
    }
    label input:checked ~ span::before {
         transform: scale(1);
    }
    label input:checked ~ span::after {
         transform: rotate(45deg)scale(1);
    }
`;