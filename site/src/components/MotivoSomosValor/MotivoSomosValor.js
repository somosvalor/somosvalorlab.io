import React from "react";
import { Link } from 'gatsby';
import Button from './../Button';

import Container from '../Container';
import * as S from './styled';

function MotivoSomosValor() {
  return (
    <S.ContainerFull>
      <S.Titulo>Por que terceirizar a cobrança com a Somos Valor?</S.Titulo>
      <S.Content>
        <S.Lista>
          <li>
            <label>
              <input type="checkbox" defaultChecked={true} disabled /> <strong>AGILIDADE:</strong> recupere seus créditos com maior agilidade;
              <span></span>
            </label>
          </li>
          <li>
            <label>
              <input type="checkbox" defaultChecked={true} disabled /> <strong>EFETIVIDADE:</strong> Aumente sua taxa de recebimento com nosso software;
              <span></span>
            </label>
          </li>
          <li>
            <label>
              <input type="checkbox" defaultChecked={true} disabled /> <strong>SEGURANÇA:</strong> Recupere seus créditos com maior agilidade;
              <span></span>
            </label>
          </li>
          <li>
            <label>
              <input type="checkbox" defaultChecked={true} disabled /> <strong>CONTROLE:</strong> Recupere seus créditos com maior agilidade;
              <span></span>
            </label>
          </li>
        </S.Lista>
      </S.Content>
    </S.ContainerFull>
  )
};

export default MotivoSomosValor;