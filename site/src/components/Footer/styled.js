import styled from 'styled-components';

import { device } from '../../configs/device';

export const Content = styled.footer`
  /* background: #0B6514; */
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 90%;
  padding-top: 25px;
  padding-bottom: 25px;
  margin: auto;

  @media ${device.mobileS} { 
    flex-direction: column;
  }
  @media ${device.mobileM} { 
    flex-direction: column;
  }
  
  @media ${device.mobileL} { 
    flex-direction: column;
  }
  
  @media ${device.tablet} { 
    flex-direction: column;
  }
  @media ${device.laptop} { 
    flex-direction: row;
  }
`;
export const ContentWrapper = styled.div`
  @media ${device.mobileS} { 
    padding: 15px 0px;
  }
  @media ${device.mobileM} { 
    padding: 15px 0px;
  }
`;
export const Titulo = styled.h2`
  text-transform: uppercase;
  text-align: center;

  @media ${device.mobileS} { 
    font-size: 18px;
  }  
  @media ${device.mobileM} { 
    font-size: 18px;
  }
  @media ${device.mobileL} { 
    font-size: 18px;
  }  
`;
export const SubTitulo = styled.h3`
  font-weight: 600;
  text-align: center;

  @media ${device.mobileS} { 
    font-size: 13px;
  }  
  @media ${device.mobileM} { 
    font-size: 13px;
  }
  @media ${device.mobileL} { 
    font-size: 13px;
  }  
`;

export const LogoSomosValor = styled.div`
  img {
    max-width: 185px;
  }
`;

export const RedeSocial = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  div {
    margin-right: 10px;

    svg{
      font-size: 35px;
      cursor: pointer;
    }
  }
`;

export const RaizLinks = styled.div`
  color: #0B6514;

  @media ${device.mobileS} { 
    text-align: center;
  }
  @media ${device.mobileM} { 
    text-align: center;
  }
  
  a{
    margin-bottom: 0px;
    font-size: 14px;
    text-decoration: none;
    display: block;
    color:#0B6514!important;
  }
`;

export const DadosSomosValor = styled.div`
  color: #0B6514;
  font-size: 12px;
  font-weight:600;

  p{
    margin-bottom: 0px;

    @media ${device.mobileS} { 
      text-align: center;
    }
    @media ${device.mobileM} { 
      text-align: center;
    }
  }

  div{
    padding-top: 20px;
  }
`;

export const WrapperBox = styled.div`
  display: grid;
  column-gap: 5px;
  row-gap: 10px;

  @media ${device.mobileS} { 
    grid-template-columns: repeat(1, minmax(168px, 1fr));
     margin-top: 20px;
  }  
  @media ${device.mobileM} { 
    grid-template-columns: repeat(1, minmax(168px, 1fr));
     margin-top: 20px;
  }
  @media ${device.mobileL} { 
    grid-template-columns: repeat(3, minmax(120px, 1fr));
     margin-top: 20px;
  }  
  @media ${device.tablet} { 
    grid-template-columns: repeat(5, minmax(120px, 1fr));
    margin-top: 20px;
  }  
  @media ${device.laptop} { 
    grid-template-columns: repeat(5,minmax(180px,1fr));
    margin-top: 20px;
  }  
`;

export const Box = styled.div`
  display: grid;
  justify-items: center;

  color: #193A1C;
`;

export const Icon = styled.img`
  @media ${device.mobileS} { 
    max-width: 168px;
    max-height: 151px;
  }  
  @media ${device.mobileM} { 
    max-width: 168px;
    max-height: 151px;
  }  
  @media ${device.mobileL} { 
    max-width: 120px;
    max-height: 151px;
  }  
  @media ${device.tablet} { 
    max-width: 120px;
    max-height: 151px;
  }  
  @media ${device.laptop} { 
    max-width: 180px;
    max-height: 151px;
  }  
`;

export const Descricao = styled.p`
  font-weight: 500;
  color: #FFFFFF;
  text-transform: uppercase;
  
  @media ${device.mobileS} { 
    font-size: 10px;
  }  
  @media ${device.mobileM} { 
    font-size: 10px;
  }  
  @media ${device.mobileL} { 
    font-size: 10px;
  }  
  @media ${device.tablet} { 
    font-size: 10px;
  }  
  @media ${device.laptop} { 
    font-size: 14px;
  }  
`;
export const Copyright = styled.div`
  background-color: #193A1C;
  color: #FFFFFF;
  text-align: center;

  min-height: 40px;
`;

export const Direitos = styled.p`
  margin-bottom: 0;
  padding-top: 13px;
  font-size: 10px; 
`;
