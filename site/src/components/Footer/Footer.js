import React from "react";

import agronegocio from '../../images/agronegocio.png';
import empresas from '../../images/empresas.png';
import industria from '../../images/industria.png';
import construcaoCivil from '../../images/construcaoCivil.png';
import sistemaEnsino from '../../images/sistemaEnsino.png';

import { FaFacebook, FaLinkedin, FaInstagram} from "react-icons/fa";

import * as S from './styled'

export default () => (
  <S.Content>
    <S.Wrapper>
      <S.ContentWrapper>
        <S.LogoSomosValor>
          <img src={'../../../images/logo.png'} alt="Somos Valor" />
        </S.LogoSomosValor>
        <S.RedeSocial>
          <div><FaFacebook /></div>
          <div><FaLinkedin /></div>
          <div><FaInstagram /></div>
        </S.RedeSocial>
      </S.ContentWrapper>
      <S.ContentWrapper>
        <S.RaizLinks>
          <h5>Sobre a Somos Valor</h5>
          <a href="/">Home</a>
          <a href="/contato" title="Seja um Parceiro da Somos Valor">Seja Parceiro</a>
          <a href="/empresas">Para Empresas</a>
          <a href="/pessoas">Para Pessoas</a>
        </S.RaizLinks>
      </S.ContentWrapper>
      <S.ContentWrapper>
        <S.DadosSomosValor>
          <p>Atual Serviços de Recuperação de créditos e Meios Digitais S.A.</p>
          <p>CNPJ: 00.000.000/0001-32</p>
          <p>Av. Pres. Juscelino Kubitschek, 2041</p>
          <p>São Paulo / SP - 04543-011</p>

          <div>
            <p>Página de Segurança | Procon | Política de Privacidade</p>
            <p>©2020 Atual | Somos Valor. Todos os direitos reservados.</p>
            <p>somosvalor@gmail.com</p>
          </div>
        </S.DadosSomosValor>
      </S.ContentWrapper>
      {/* <S.Titulo>Atendemos atacado e varejo</S.Titulo>
      <S.SubTitulo>Aqui nossa maior satisfação é ver você feliz com nossos resultados.</S.SubTitulo>

      <S.WrapperBox>
        <S.Box>
          <S.Icon src={'../../images/empresas.png'}></S.Icon>
          <S.Descricao>Agronegócio</S.Descricao>
        </S.Box>
        <S.Box>
          <S.Icon src={'../../images/agronegocio.png'}></S.Icon>
          <S.Descricao>Empresas</S.Descricao>
        </S.Box>
        <S.Box>
          <S.Icon src={"../../images/agronegocio.png"}></S.Icon>
          <S.Descricao>Indústria</S.Descricao>
        </S.Box>
        <S.Box>
          <S.Icon src={"../../images/construcaoCivil.png"}></S.Icon>
          <S.Descricao>Construção Civil</S.Descricao>
        </S.Box>
        <S.Box>
          <S.Icon src={"../../images/sistemaEnsino.png"}></S.Icon>
          <S.Descricao>Sistema de Ensino</S.Descricao>
        </S.Box>
      </S.WrapperBox>
    
    <S.Copyright>
        <S.Direitos>Copyright © 2020 SOMOS VALOR. Todos os direitos reservados.</S.Direitos>
    </S.Copyright> */}
    </S.Wrapper>
  </S.Content>  
);