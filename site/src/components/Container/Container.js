import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  padding: 20px 0px;
  margin: 0 auto;
`;

export default Container;
