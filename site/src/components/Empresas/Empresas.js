import React from "react";
import { Form } from 'react-bootstrap';

import Container from '../Container';
import * as S from './styled';

import pageEmpresas from '../../images/pageEmpresas.png';

function Empresas() {

  return (
    <Container>
      <S.Wrapper>
        <div>
          <S.Titulo>Para empresas</S.Titulo>
          <S.Text>
            Cada vez mais os administradores compreendem a importância de um sistema de cobrança automatizado. Afinal, os benefícios que esse tipo de solução oferece agradam empresas e clientes. Portanto, ajuda a facilitar a rotina dos negócios e também as formas de pagamento.
          </S.Text>
          <S.Text>
            Formas tradicionais de cobrança, principalmente aquelas que dependem dos bancos, muitas vezes significam mais problemas do que soluções para a organização. A automatização, com suporte de softwares específicos, simplifica as transações. Além disso, oferece a segurança que empresas e clientes precisam, além de permitir uma gestão muito mais eficiente.
          </S.Text>
          <S.Text>
            Nosso objetivo é recuperar o crédito e manter seu cliente, e para isso, priorizamos a cobrança amigável e estamos sempre nos atualizando com as novas exigências de mercado. 
          </S.Text>
          <S.Text>
            Possuímos uma equipe altamente capacitada para realizar com excelência todos os procedimentos da nossa régua de cobrança, visando uma maior taxa de efetividade para sua empresa.
          </S.Text>
        </div>
        <div>
          <img src={'../../images/pageEmpresas.png'} alt="Para Empresas" />
        </div>
      </S.Wrapper>
    </Container>
  )
}

export default Empresas;
