import styled from 'styled-components';

import { device } from '../../configs/device';

export const Wrapper = styled.div`
  display: grid;
  padding: 40px 0px;
  column-gap: 40px;
  align-content: center;
  align-items: center;  
  color: #0B6514;

  @media ${device.mobileS} { 
    grid-template-columns: repeat(1, minmax(267px, 1fr));
  }  
  @media ${device.mobileM} { 
    grid-template-columns: repeat(1, minmax(267px, 1fr));
  }
  @media ${device.mobileL} { 
    grid-template-columns: repeat(1, minmax(267px, 1fr));
  }  
  @media ${device.tablet} { 
    grid-template-columns: repeat(2, minmax(267px, 1fr));
  }  
  @media ${device.laptop} { 
    grid-template-columns: repeat(2, minmax(367px, 1fr));
  }  
`;

export const Titulo = styled.h2`
  margin-top: 15px;
  font-weight: bold;  
  text-transform: uppercase;
  text-align: center;
`;

export const Text = styled.p`
  text-align: justify;
`;
