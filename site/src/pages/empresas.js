import React from 'react';
import { Link } from 'gatsby';

import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from '../components/Layout';
import SEO from '../components/SEO';
import EmpresasContent from '../components/Empresas';

const Empresas = () => (
  <Layout>
    <SEO title="Empresas" />
    <EmpresasContent />
  </Layout>
);

export default Empresas;
