import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/Layout';
import Image from '../components/Image';
import SEO from '../components/SEO';
import Carousel from '../components/Carousel';
import NegocieDivida from '../components/NegocieDivida';
import MotivoSomosValor from '../components/MotivoSomosValor';
import Time from '../components/Time';
import Parceiros from '../components/Parceiros';
import Solucoes from '../components/Solucoes';
import About from '../components/About';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Carousel />
    <Parceiros />
    <About />
    <Solucoes />
    <NegocieDivida />
    <MotivoSomosValor />
    <Time />
    {/* <Link to="/page-2/">Go to page 2</Link> */}
  </Layout>
);

export default IndexPage;
