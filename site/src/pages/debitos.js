import React from 'react';
import { Link } from 'gatsby';

import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from '../components/Layout';
import SEO from '../components/SEO';
import Banner from '../components/Central/Debitos/Banner';
import DebitosContent from '../components/Central/Debitos';

const Debitos = () => (
  <Layout>
    <SEO title="Débitos" />
    <Banner />
    <DebitosContent />
  </Layout>
);

export default Debitos;
