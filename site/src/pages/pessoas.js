import React from 'react';
import { Link } from 'gatsby';

import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from '../components/Layout';
import SEO from '../components/SEO';
import PessoasContent from '../components/Pessoas';

const Pessoas = () => (
  <Layout>
    <SEO title="Para Pessoas" />
    <PessoasContent />
  </Layout>
);

export default Pessoas;
