import React from 'react';
import { Link } from 'gatsby';

import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from '../components/Layout';
import SEO from '../components/SEO';
import FormContato from '../components/FormContato';

const Contato = () => (
  <Layout>
    <SEO title="Contato" />
    <FormContato />
  </Layout>
);

export default Contato;
