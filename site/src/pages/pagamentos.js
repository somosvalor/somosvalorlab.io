import React from 'react';
import { Link } from 'gatsby';

import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from '../components/Layout';
import SEO from '../components/SEO';
import Banner from '../components/Central/Pagamentos/Banner';
import PagamentosContent from '../components/Central/Pagamentos';

const Pagamentos = () => (
  <Layout>
    <SEO title="Pagamentos" />
    <Banner />
    <PagamentosContent />
  </Layout>
);

export default Pagamentos;
